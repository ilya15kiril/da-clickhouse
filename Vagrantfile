# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/jammy64"

  config.vm.hostname = "vm.da-clickhouse.nuft.edu.ua"
  config.vm.define "vm.da-clickhouse.nuft.edu.ua"
  config.vm.provider "virtualbox" do |v|
    v.name = "vm.da-clickhouse.nuft.edu.ua"
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  config.vm.network "forwarded_port", guest: 8081, host: 8081, host_ip: "127.0.1.1"
  config.vm.network "forwarded_port", guest: 8082, host: 8082, host_ip: "127.0.1.1"
  config.vm.network "forwarded_port", guest: 8123, host: 8123, host_ip: "127.0.1.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Disable the default share of the current code directory. Doing this
  # provides improved isolation between the vagrant box and your host
  # by making sure your Vagrantfile isn't accessable to the vagrant box.
  # If you use this you may want to enable additional shared subfolders as
  # shown above.
  # config.vm.synced_folder ".", "/vagrant", disabled: true

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
    vb.memory = "2048"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    # ========
    # Setup OS
    # ========
    timedatectl list-timezones | grep Kyiv
    timedatectl set-timezone Europe/Kyiv
    sed -i 's/#DNS=/DNS=8.8.8.8/g' /etc/systemd/resolved.conf
    sed -i 's/#FallbackDNS=/FallbackDNS=8.8.4.4/g' /etc/systemd/resolved.conf
    systemctl restart systemd-resolved

    # =================
    # Init repositories
    # =================
    apt update
    apt install -y curl wget apt-transport-https ca-certificates software-properties-common dirmngr openjdk-11-jre-headless nginx
    GNUPGHOME=$(mktemp -d)
    GNUPGHOME="$GNUPGHOME" gpg --no-default-keyring --keyring /etc/apt/keyrings/clickhouse-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8919F6BD2B48D754
    rm -rf "$GNUPGHOME"
    chmod +r /etc/apt/keyrings/clickhouse-keyring.gpg
    echo "deb [signed-by=/etc/apt/keyrings/clickhouse-keyring.gpg] https://packages.clickhouse.com/deb stable main" | tee /etc/apt/sources.list.d/clickhouse.list

    mkdir -p /etc/apt/keyrings/
    wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
    echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | tee -a /etc/apt/sources.list.d/grafana.list

    # =========================
    # Install required software
    # =========================
    apt-get update
    export DEBIAN_FRONTEND=noninteractive
    apt install -y clickhouse-server clickhouse-client
    cp /vagrant/clickhouse/config.xml /etc/clickhouse-server/config.xml
    systemctl daemon-reload
    systemctl start clickhouse-server
    systemctl enable clickhouse-server

    apt install -y grafana
    cp /vagrant/grafana/grafana.ini /etc/grafana/grafana.ini
    grafana-cli plugins install grafana-clickhouse-datasource
    systemctl daemon-reload
    systemctl start grafana-server.service
    systemctl enable grafana-server.service

    groupadd -r metabase &&  useradd -r -s /bin/false -g metabase metabase
    mkdir -p /srv/metabase/plugins && cd /srv/metabase
    curl -o metabase.jar https://downloads.metabase.com/v0.48.1/metabase.jar
    curl -L -o plugins/ch.jar https://github.com/ClickHouse/metabase-clickhouse-driver/releases/download/1.3.1/clickhouse.metabase-driver.jar
    chown -R metabase:metabase /srv/metabase
    touch /var/log/metabase.log && chown syslog:adm /var/log/metabase.log
    cp /vagrant/metabase/metabase.conf /etc/metabase && chmod 640 /etc/metabase
    cp /vagrant/metabase/metabase.service /etc/systemd/system/metabase.service
    cp /vagrant/metabase/rsyslog.conf /etc/rsyslog.d/metabase.conf
    systemctl daemon-reload
    systemctl restart rsyslog.service
    systemctl start metabase.service
    systemctl enable metabase.service

    rm -f /etc/nginx/sites-enabled/default
    cp /vagrant/metabase/nginx.conf /etc/nginx/sites-enabled/metabase.conf
    cp /vagrant/grafana/nginx.conf /etc/nginx/sites-enabled/grafana.conf
    systemctl restart nginx.service

    # ==============
    # Check services
    # ==============
    systemctl status clickhouse-server
    sleep 1
    systemctl status metabase.service
    sleep 1
    systemctl status grafana-server.service
    sleep 1
    systemctl status nginx.service

    # =============
    # Init database
    # =============
  SHELL

  config.vm.provision "shell", run: "always", inline: <<-SHELL
    # ======
    # README
    # ======
    echo "+------------+-----------------------+---------+----------+"
    echo "|  Service   |          URL          |  Login  | Password |"
    echo "+------------+-----------------------+---------+----------+"
    echo "| ClickHouse | 127.0.1.1:8123        | default | (empty)  |"
    echo "| Metabase   | http://127.0.1.1:8081 |         |          |"
    echo "| Grafana    | http://127.0.1.1:8082 | admin   | admin    |"
    echo "+------------+-----------------------+---------+----------+"
  SHELL
end
